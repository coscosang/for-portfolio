import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-chat-search',
  templateUrl: './chat-search.component.html',
  styleUrls: ['./chat-search.component.css']
})
export class ChatSearchComponent implements OnInit {

  @Input() Service: any;
  Form: FormGroup;
  constructor() { }

  ngOnInit() {
    this.initForms();
  }
  initForms() {
    this.Form = new FormGroup({
      q:  new FormControl('', Validators.minLength(3)),
    });
  }

  onSubmit() {
    const controls = this.Form.controls;
    const values = this.Form.value;

    // Проверяем форму на валидность */
    if (this.Form.invalid) {
      // Если форма не валидна, помечаем все контролы как touched **/
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }
    this.Service.getGlobalSearch(values.q, 150).subscribe((result: any) => {

      },
      error => {
        this.Service.errorHandling(error);
      });
  }
}
