import {
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { LocalStorageService, SessionStorageService, LocalStorage, SessionStorage } from 'angular-web-storage';
import {isUndefined} from 'util';
import {from, Subscription} from 'rxjs';
import {filter} from 'rxjs/operators';
import {TelegramService} from '../page-telegram/telegram.service';
import {WebsocketService} from '../../services/websocket.service';
import {NotifierWrapService} from '../../services/notifier-wrap.service';
import {AppComponent} from '../../app.component';
import { TimeagoIntl } from 'ngx-timeago';
import {strings as rusStrings} from 'ngx-timeago/language-strings/ru';
import {ContextMenuComponent, ContextMenuService} from "ngx-contextmenu";
import {tag} from "rxjs-spy/cjs/operators";
@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatListComponent implements OnInit, OnDestroy {
  public isLoaded = false;
  public isLoadError = false;
  @Input() type: string;
  @Input() Service: any;
  @Output() onSettings = new EventEmitter<any>();
  onlyUnread: boolean = false;
  loadResultsTimeout: any;
  sortChatsSubscription: Subscription;
  loadResultsSubscription: Subscription;
  private reloadRequestsSubscription: Subscription;
  private onlineStatusSubscription: Subscription;
  @ViewChild(ContextMenuComponent) public contextMenu: ContextMenuComponent;
  constructor(
    public contextMenuService: ContextMenuService,
    private cd: ChangeDetectorRef,intl: TimeagoIntl,
    public wsService: WebsocketService, public appComponent: AppComponent,
    public telegramService: TelegramService, public local: LocalStorageService,
    public session: SessionStorageService) {
    console.log(rusStrings);
    intl.strings = rusStrings;
    intl.changes.next();

    this.onlineStatusSubscription = this.wsService.onlineStatus.asObservable().pipe(tag("onlineStatus")).subscribe((result) => {
      if (this.isLoaded){
        from(this.Service.arChat).pipe(
          filter((v: any) => v.id === result.user_id)
        ).subscribe((chat: any) => {
          chat.online = result.online;
          if (!chat.online){
            chat.typing = false;
          }
          chat.last_seen = result.last_seen;
          this.cd.detectChanges();
        });
      }
    });
    this.wsService.typingUser.asObservable().pipe(tag("typingUser")).subscribe((result) => {
      if (this.isLoaded){
        from(this.Service.arChat).pipe(
          filter((v: any) => v.id === result.user_id)
        ).subscribe((chat: any) => {
          chat.typing = true;
          setTimeout(() => {
            chat.typing = false;
            this.cd.detectChanges();
          }, 5000);
          this.cd.detectChanges();
        });
      }
    });
  }
  ngOnInit(): void {
   // this.cd.detach();
    this.reload();
    this.wsService.newMessage.asObservable().pipe(tag("newMessage")).subscribe((resultMessage) => {
      !isUndefined(resultMessage.new_message) && this.onNewMesage(resultMessage.new_message);
    });
    this.reloadRequestsSubscription = this.wsService.reloadRequests$.asObservable().pipe(tag("reloadRequests$")).subscribe(val => {
      this.isLoadError && this.reload();
    });
    this.sortChatsSubscription = this.Service.newMessage$.asObservable().pipe(tag("newMessage$")).subscribe(resultMessage => {
      resultMessage && this.onNewMesage(resultMessage);
    });
  }
  /// Context Menu
  public onContextMenu(item: any) {
    console.log(item);
  }
  public contextMenuOpen($event, item: any): void {
    this.contextMenuService.show.next({
      anchorElement: $event.target,
      // Optional - if unspecified, all context menu components will open
      contextMenu: this.contextMenu,
      event: <any>$event,
      item: item,
    });
    $event.preventDefault();
    $event.stopPropagation();
  }
  public isChatPinned(item: any): boolean {
    return item.is_pinned;
  }
  public isNotChatPinned(item: any): boolean {
    return !item.is_pinned;
  }
  /////////
  onNewMesage(newMessage: any) {
    var chatExist = false;
    from(this.Service.arChat).pipe(
      filter((v: any) => v.id === newMessage.chat_id)
    ).subscribe((chat: any) => {
      chat.typing = false;
      chat.last_message = newMessage;
      if (!chat.last_message.out) {
        chat.unread_messages_count++;
      }else{
        chat.unread_messages_count = 0;
      }
      this.chatsResort();
      chatExist = true;
    });

    if(!chatExist){
      this.Service.chatList().subscribe((result: any) => {
          if (isUndefined(result.error_msg)){
            from(result.results).pipe(
              filter((chat: any) => chat.id == newMessage.chat_id)
            ).subscribe((chat: any) => {
              chat.color = this.appComponent.stringToHslColor(chat.id);
              chat.isSelf = this.Service.arProfile.id == chat.id;
              this.Service.arChat.push(chat);
              this.chatsResort();
            });
          }
          this.Service.chatsLocal = this.Service.arChat;
          //this.cd.detectChanges();
        },
        error => {
          this.Service.errorHandling(error);
        });
    }
    this.Service.chatsLocal = this.Service.arChat;
    this.cd.detectChanges();
  }
  chatsResort(){
    this.Service.arChat = this.appComponent.chatSort(this.Service.arChat);
    this.cd.detectChanges();
  }
  reload(){
    this.loadResultsSubscription && this.loadResultsSubscription.unsubscribe();
    const chatsLocal = this.Service.chatsLocal;
    if(!chatsLocal || Object.keys(chatsLocal)?.length == 0){
      this.isLoaded = false;
      this.loadResultsSubscription = this.loadChats();
    }else {
      this.isLoaded = true;
      this.Service.arChat = chatsLocal;
      setTimeout(() => {
        this.loadResultsSubscription = this.loadChats();
      }, 3000)
    }
    this.cd.detectChanges();
  }

  ngOnDestroy(){
    this.loadResultsSubscription && this.loadResultsSubscription.unsubscribe();
    this.sortChatsSubscription.unsubscribe();
    this.reloadRequestsSubscription.unsubscribe();
    this.onlineStatusSubscription.unsubscribe();
  }
  trackByFn(index, element: any) {
    return element? element.id : undefined;
  }
  loadChats(){
    this.isLoadError = false;

    return this.Service.chatList().subscribe((result: any) => {
        if (isUndefined(result.error_msg)){
          var i = 0;
          from(result.results).pipe(
            filter((chat: any) => chat.id)
          ).subscribe((chat: any) => {
            i++;
            chat.color = this.appComponent.stringToHslColor(chat.id);
            chat.isSelf = this.Service.arProfile.id == chat.id;
          });
          //result.results.length = 5

          this.Service.arChat = this.Service.chatsLocal = result.results;
          this.isLoaded = true;
        }else{
          this.isLoadError = result.error_msg;
          alert(result.error_msg);
        }
        this.cd.detectChanges();
      },
      error => {
        this.isLoaded = false;
        this.isLoadError = error;
        this.cd.detectChanges();
      });
  }
  openChat(index, chat: any){
    this.Service.openChatId = chat.id;
    this.cd.detectChanges();
  }
  openSettings(){
    this.onSettings.emit(true);
  }
  get runChangeDetection() {
    console.log('ChatListComponent - Checking the view');
    return '';
  }
}
