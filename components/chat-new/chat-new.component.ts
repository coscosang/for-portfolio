import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input, OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import {TelegramService} from "../page-telegram/telegram.service";
import {ModalService} from "../../services/modal.service";
import {isObject, isUndefined} from "util";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {NotifierWrapService} from "../../services/notifier-wrap.service";
import {WebsocketService} from "../../services/websocket.service";
import {AppComponent} from "../../app.component";
import {from, Subscription} from "rxjs";
import {filter} from "rxjs/operators";
import {tag} from "rxjs-spy/cjs/operators";

@Component({
  selector: 'app-chat-new',
  templateUrl: './chat-new.component.html',
  styleUrls: ['./chat-new.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatNewComponent implements OnInit, OnDestroy {
  public isLoadError = false;
  getAvalibleTimeout: any;
  loadResultsSubscription: Subscription;
  @Output() onClose = new EventEmitter<any>();
  Form: FormGroup;
  arResult: any = [];
  public isLoaded = false;
  @Input() Service: any;
  usersSelect: any = {};
  existUserId: any;
  dismissible = true;
  defaultAlerts: any[] = [
    {
      type: 'success',
    },
  ];
  alerts = this.defaultAlerts;
  reloadRequestsSubscription: Subscription;
  onlineStatusSubscription: Subscription;
  constructor(
    private cd: ChangeDetectorRef,
    public telegramService: TelegramService, public modalService: ModalService,
    private notifierWrapService: NotifierWrapService, public appComponent: AppComponent,
    private wsService: WebsocketService
  ) {

    this.onlineStatusSubscription = this.wsService.onlineStatus.asObservable().pipe(tag("onlineStatus")).subscribe((result) => {
      if (this.isLoaded){
        from(this.arResult).pipe(
          filter((v: any) => v.id === result.user_id)
        ).subscribe((chat: any) => {
          chat.online = result.online;
          if (!chat.online){
            chat.typing = false;
          }
          chat.last_seen = result.last_seen;
        });
        this.cd.detectChanges();
      }
    });
  }

  ngOnInit(): void {
    this.reloadRequestsSubscription = this.wsService.reloadRequests$.asObservable().pipe(tag("reloadRequests$")).subscribe(val => {
      this.isLoadError && val && this.loadContacts(true);
    });
    this.initForms();
  }
  ngOnDestroy(){
    this.reloadRequestsSubscription.unsubscribe();
    this.onlineStatusSubscription.unsubscribe();
  }
  addTitleList(arContacts: any){
    if(isObject(arContacts)){
      for (let i = 0; i < arContacts.length; i++) {
        arContacts[i].first_character = (arContacts[i].name.first || arContacts[i].name.last).substring(0,1).toLowerCase();
        arContacts[i].shiw_title = (i == 0 || (arContacts[i-1] && arContacts[i-1].first_character !== arContacts[i].first_character));
      }
    }
  }
  loadContacts(vizible: boolean, timeout = 0){
    if(!vizible || this.arResult?.length > 0)return;
    this.isLoadError = false;
    this.getAvalibleTimeout = setTimeout(() => {
      this.loadResultsSubscription && this.loadResultsSubscription.unsubscribe();
      this.loadResultsSubscription = this.Service.contactsList(this.Form.get('q').value).subscribe((result: any) => {
          if (isUndefined(result.error_msg)) {
            this.arSort(result.results);
            this.addTitleList(result.results);
            this.arResult = result.results;
            this.isLoaded = true;
            this.cd.detectChanges();
          } else {
            alert(result.error_msg);
          }
          this.cd.detectChanges();
        },
        error => {
          this.cd.detectChanges();
          this.isLoadError = error;
        });
    }, timeout);
  }
  toggleUser(event, client): void {
    if(isUndefined(this.usersSelect[client.id])){
      client.checked = true;
      this.usersSelect[client.id] = client;
      if(this.usersSelectCount == 1){
        from(this.Service.arChat).pipe(
          filter((v: any) => v.id === client.id)
        ).subscribe((chat: any) => {
          this.existUserId = chat.id;
        });
        this.cd.detectChanges();
      }else{
        delete this.existUserId;
        this.cd.detectChanges();
      }

    }else{
      this.cd.detectChanges();
      client.checked = false;
      delete this.existUserId;
      delete this.usersSelect[client.id];
    }
  }
  get usersSelectCount() {
    return Object.keys(this.usersSelect).length;
  }
  initForms() {
    this.Form = new FormGroup({
      title: new FormControl('', [Validators.required]),
      description: new FormControl(''),
      q: new FormControl(''),
    });
    this.Form.get('q').valueChanges.subscribe(val => {
      this.arResult = [];
      this.loadContacts(true);
    })
  }

  onSubmit() {
    const controls = this.Form.controls;
    const values = this.Form.value;

    // Проверяем форму на валидность */
    if (this.Form.invalid) {
      // Если форма не валидна, помечаем все контролы как touched **/
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());

      this.notifierWrapService.notify('error', 'Укажите название беседы');
      return;
    }
    this.Service.chatNew(values.title, Object.keys(this.usersSelect)).subscribe((result: any) => {
        this.Service.arChat.push(result);
        this.Service.arChat = this.appComponent.chatSort(this.Service.arChat);
        this.openChat(result.id);
        this.notifierWrapService.notify('info', 'Беседа создана');
        this.cd.detectChanges();
      },
      error => {
        this.cd.detectChanges();
        this.Service.errorHandling(error);
      });

  }
  openChat(id){
    this.Service.openChatId = id;
    this.onClose.emit(true);
    this.cd.detectChanges();
  }

  arSort(array: any): any {
    return array.sort((a: any, b: any) => {
      const aValue = a.name.first.toLowerCase();
      const bValue = b.name.first.toLowerCase();

      if(!bValue)
        return -1;
      return aValue < bValue ? -1 : bValue < aValue ? 1 : 0;
    });
  }
  onClosed(dismissedAlert: any): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}
